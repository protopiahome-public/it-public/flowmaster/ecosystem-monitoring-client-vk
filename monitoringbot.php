<?php

use \Firebase\JWT\JWT;

error_reporting(0);
echo "ok";
flush();
function output_null(){}
ini_set("output_handler", "output_null");
ini_set("dipslay_errors", 0);
require_once("config.php");
require_once("vk_functions.php");
require_once("graphql_functions.php");

$data = json_decode(file_get_contents('php://input'), true);
file_put_contents(__DIR__ . "/log.txt", print_r($_REQUEST, true), FILE_APPEND);
file_put_contents(__DIR__ . "/log.txt", print_r($data, true), FILE_APPEND);

if ($data["secret"] != $vk_secret || $data["type"] != "message_new")
{
	die();
}
$data = $data["object"];

protopia_auth();

$data["text"] = preg_replace("#^(\/[a-z0-9\_]+)\@.+?bot#i", "\\1", $data["text"]);
session_id("monitoringvk" . $data["from_id"]);
session_start();

if (!$ecosystem_user_token && preg_match("#^\/connect (.+)$#s", $data["text"], $matches))
{
	$id_token = array(
		"iss" => "vk.com",
		"sub" => $data["from_id"],
		"aud" => "",
		"exp" => "",
		"nonce" => "",
		"acr" => "vk",
		"amr" => "mca",
	);
	$jwt = JWT::encode($id_token, $jwt_key);
	
	$token = protopia_mutation("authorize", "token", ["input: AuthorizeInput!" => 
	array(
		"response_type" => "token",
		"client_id" => $ecosystem_client_id,
		"scope" => "user",
		"user_code" => $matches[1],
		"login_hint_token" => $jwt,
		)
	]);
	
	answer("Вы авторизовались!");
}
elseif (false && !$ecosystem_user_token && preg_match("#^\/register$#s", $data["text"], $matches))
{
	protopia_mutation("registerUser", "_id", ["input: UserInput" => [
		"name" => $data["message"]["from"]["first_name"],
		"family_name" => $data["message"]["from"]["last_name"],
		"vk_id" => $data["from_id"],
	]]);
}
elseif (!$ecosystem_user_token && preg_match("#^\/#s", $data["text"], $matches))
{
	answer("@username, добро пожаловать в чат-мониторинг систему. 
Если вы уже пользовались системой в другой соц сети, то можете связать аккаунты в соц. сетях, написав “/connect” в личную переписку боту, получив одноразовый пароль для ввода в другой системе.
Если вы хотите войти в веб-панель, то введите команду “/panel” в личную переписку боту и получите одноразовый пароль для этого. Затем вы сможете в веб-панели сменить его на постоянный.
Если вы хотите просто продолжить пользоваться ботом, введите вашу команду еще раз. В этом случае, если вы уже имеете аккаунт, то вам придется синхронизировать аккаунты в соц. сетях вручную.");
}


if (preg_match("#^\/panel$#s", $data["text"], $matches))
{
	$result = protopia_mutation("associate", "user_code", [
		"input: AuthenticatorInput" => ["authenticator_type" => "otp"],
	]);
	
	answer("Вы можете войти в веб-панель по адресу http://monitoring-panel.kb.protopia-home.ru?action=auth_code&code={$result["user_code"]}");
}
elseif (preg_match("#^\/help$#s", $data["text"], $matches))
{
	answer(print_r(get_user($data["peer_id"], $data["from_id"]), true));
	$user = _vkApi_call('users.get', array( 
			'user_ids' => $data["from_id"], 
		));
	answer(print_r($user, true));
	return;
	
	answer("/themes - список доступных мне тем
/sources - список доступных мне источников
/shared_sources - список источников, расшаренных для данного приемника
/receivers - список доступных мне приемников
/monitor - сделать из чата источник
/receive - сделать из чата приемник
/create_theme <название> - создать тему для текущего приемника
/add_theme <тема> - добавить (скопировать) тему в текущий приемник
/copy_theme_to <тема> <receiver> - скопировать тему в указанный приемник
/activate_source <источник> - включить подписку приемника на источник
/activate_theme <тема> - включить подписку приемника на тему
/deactivate_source <источник> - отключить подписку приемника на источник
/deactivate_theme <тема> - отключить подписку приемника на тему
/change_theme <тема> <название> - изменить название темы
/add_word_theme <тема> <слово> - добавить слово в тему
/remove_word_theme <тема> <слово> - удалить слово из темы
/delete_theme <тема> - удалить тему
/receiver_info - получить список тем и источников для данного приемника
/share_receiver - получить ссылку на приемник
/share <приемник> - расшарить текущий чат-источник указанному приемнику
/unshare_to <приемник> - отменить расшаривание текущего чат-источника приемнику
/unshare <источник> - отменить расшаривание указанного чат-источника текущему приемнику
/cancel_share <источник> - отказаться от расшаривания указанного источника в текущий приемник
/shared_to - показать список приемников, которым расшарен текущий источник
/panel - получить код доступа в веб-панель
/connect <code> - ввести код доступа для связывания аккаунтов
/source_public - сделать источник публичным
/receiver_public - сделать приемник публичным
/theme_public <тема> - сделать тему публичной
/source_unpublic - сделать источник непубличным
/receiver_unpublic - сделать приемник непубличным
/theme_unpublic <тема> - сделать тему непубличной
/share_to <приемник> - расшарить источник в указанный приемник
");
}
elseif (preg_match("#^\/create_theme (.+)$#s", $data["text"], $matches))
{
	protopia_mutation("changeTheme", "_id", ["receiver_id: ID" => getReceiverByExternal(), "input: ThemeInput" => [
		"title" => $matches[1],
	]]);
	
	answer("Тема создана.");
}
elseif (preg_match("#^\/add_theme (.+)$#s", $data["text"], $matches))
{
	protopia_mutation("copyThemeToReceiver", "_id", [
		"_id: ID" => getIdByLinkOrTitle($matches[1], "theme"),
		"receiver_id: ID" => getReceiverByExternal(),
	]);
	
	answer("Тема скопирована.");

}
elseif (preg_match("#^\/change_theme (.+) (.+)$#s", $data["text"], $matches))
{
	protopia_mutation("changeTheme", "_id", [
		"_id: ID" => getIdByLinkOrTitle($matches[1], "theme"),
		"input: ThemeInput" => [
			"title" => $matches[2],
	]]);
	
	answer("Тема изменена.");
}
elseif (preg_match("#^\/add_word_theme (.+) (.+)$#s", $data["text"], $matches))
{
	protopia_mutation("addKeyWordToTheme", "_id", [
		"theme_id: ID" => getIdByLinkOrTitle($matches[1], "theme"),
		"keyword: String" => $matches[2],
	]);
	
	answer("Ключевое слово добавлено.");
}
elseif (preg_match("#^\/remove_word_theme (.+) (.+)$#s", $data["text"], $matches))
{
	protopia_mutation("removeKeyWordFromTheme", "_id", [
		"theme_id: ID" => getIdByLinkOrTitle($matches[1], "theme"),
		"keyword: String" => $matches[2],
	]);
	
	answer("Ключевое слово удалено.");
}
elseif (preg_match("#^\/delete_theme (.+)$#s", $data["text"], $matches))
{
	protopia_mutation("deleteTheme", "_id", [
		"_id: ID" => getIdByLinkOrTitle($matches[1], "theme"),
	]);
	
	answer("Тема удалена.");
}
elseif (preg_match("#^\/cancel_share (.+)$#s", $data["text"], $matches))
{
	protopia_mutation("unshareSource", "_id", [
		"source_id: ID" => getIdByLinkOrTitle($matches[1], "source"),
		"receiver_id: ID" => getReceiverByExternal($data["peer_id"]),
	]);
	
	answer("Вы отказались от источника.");
}
elseif (preg_match("#^\/shared_to$#s", $data["text"], $matches))
{
	$text = "";
	
	$source = protopia_query("getSource", "_id shared_to_receivers {_id title}", ["id: ID!" => getSourceByExternal()]);
	
	if ($source)
	{
		$text = "";
		if ($source["shared_to_receivers"])
		{
			$text .= "Приемники:\n";
			foreach ($source["shared_to_receivers"] as $receiver)
			{
				$text .= "----{$receiver["title"]}\n";
			}
		}
	}
	if (!count($source["shared_to_receivers"]))
	{
		$text = "Приемники отсутствуют.";
	}
	
	answer($text);
}
elseif (preg_match("#^\/connect_to$#s", $data["text"], $matches))
{
//	$ecosystem_client_auth = true;
	$result = protopia_mutation("associate", "user_code", ["input: AuthenticatorInput" => [
		"authenticator_type" => "oob",
		"oob_channel" => "vk",
		"external_id" => $data["from_id"],
	]]);
//	$ecosystem_client_auth = false;
	answer("Пожалуйста, введите в другой системе команду \"/connect_to {$result["user_code"]}\"");
}
elseif (preg_match("#^\/source_public$#s", $data["text"], $matches))
{
	protopia_mutation("changeSource", "_id", [
		"_id: ID" => getSourceByExternal(),
		"input: SourceInput" => [
			"is_public" => true,
		]
	]);
	
	answer("Источник опубликован.");
}
elseif (preg_match("#^\/receiver_public$#s", $data["text"], $matches))
{
	protopia_mutation("changeReceiver", "_id", [
		"_id: ID" => getReceiverByExternal(),
		"input: ReceiverInput" => [
			"is_public" => true,
		]
	]);
	
	answer("Приемник опубликован.");
}
elseif (preg_match("#^\/theme_public (.+)$#s", $data["text"], $matches))
{
	protopia_mutation("changeTheme", "_id", [
		"_id: ID" => getIdByLinkOrTitle($matches[1], "theme"),
		"input: ThemeInput" => [
			"is_public" => true,
		]
	]);
	
	answer("Тема опубликована.");
}
elseif (preg_match("#^\/source_unpublic$#s", $data["text"], $matches))
{
	protopia_mutation("changeSource", "_id", [
		"_id: ID" => getSourceByExternal(),
		"input: SourceInput" => [
			"is_public" => false,
		]
	]);
	
	answer("Источник распубликован.");
}
elseif (preg_match("#^\/receiver_unpublic$#s", $data["text"], $matches))
{
	protopia_mutation("changeReceiver", "_id", [
		"_id: ID" => getReceiverByExternal(),
		"input: ReceiverInput" => [
			"is_public" => false,
		]
	]);
	
	answer("Приемник распубликован.");
}
elseif (preg_match("#^\/theme_unpublic (.+)$#s", $data["text"], $matches))
{
	protopia_mutation("changeTheme", "_id", [
		"_id: ID" => getIdByLinkOrTitle($matches[1], "theme"),
		"input: ThemeInput" => [
			"is_public" => false,
		]
	]);
	
	answer("Тема распубликована.");
}
elseif (preg_match("#^\/themes$#s", $data["text"], $matches))
{
	$themes = protopia_query("getThemes", "_id title activate keywords receiver {title external_type}");
	$text = "";
	foreach ($themes as $theme)
	{
		$theme["receiver"]["title"] = $theme["receiver"]["title"] ? $theme["receiver"]["title"] . ":" : "";
		$status = $theme["activate"] ? "активна" : "неактивна";
		$text .= "{$theme["receiver"]["title"]}{$theme["receiver"]["external_type"]}:{$theme["title"]} (" . implode(", ", $theme["keywords"]) . ") [" . $status . "]\n";
	}
	if (!count($themes))
	{
		$text = "Темы отсутствуют.";
	}
	answer($text);
}
elseif (preg_match("#^\/sources$#s", $data["text"], $matches))
{
	$sources = protopia_query("getSources", "_id title external_system");
	
	$text = "";
	
	foreach ($sources as $source)
	{
		$source["external_id"] = preg_replace("#^\-100#", "", $source["external_id"]);
		$text .= "{$source["title"]} ({$source["external_system"]})\n";
	}
	
	if (!count($sources))
	{
		$text = "Источники отсутствуют.";
	}
	
	answer($text);
}
elseif (preg_match("#^\/receivers$#s", $data["text"], $matches))
{
	$sources = protopia_query("getReceivers", "_id title external_system");

	$text = "";
	
	foreach ($sources as $source)
	{
		$source["external_id"] = preg_replace("#^\-100#", "", $source["external_id"]);
		$text .= "{$source["title"]} ({$source["external_system"]})\n";
	}
	
	if (!count($sources))
	{
		$text = "Приемники отсутствуют.";
	}
	
	answer($text);
}
elseif (preg_match("#^\/shared_sources$#s", $data["text"], $matches))
{
	$sources = protopia_query("getSharedSourcesByReceiver", "_id title external_id", ["receiver_id: ID" => getReceiverByExternal()]);

	$text = "";
	
	foreach ($sources as $source)
	{
		$source["external_id"] = preg_replace("#^\-100#", "", $source["external_id"]);
		$text .= "{$source["title"]} http://t.me/c/{$source["external_id"]}/999999\n";
	}
	
	if (!count($sources))
	{
		$text = "Расшаренные источники отсутствуют.";
	}

	answer($text);
}
elseif (preg_match("#^\/monitor$#s", $data["text"], $matches))
{
	protopia_mutation("changeSource", "_id", ["input: SourceInput" => [
		"external_id" => $data["peer_id"],
		"external_type" => $data["peer_id"] == $data["from_id"] ? "personal_chat" : "group_chat",
		"external_system" => "vk",
		"title" => get_chat($data["peer_id"])["chat_settings"]["title"],
	]]);
	
	answer("Этот чат теперь мониторится. Его можно использовать как источник информации.");
}
elseif (preg_match("#^\/receive$#s", $data["text"], $matches))
{
	$receiver = protopia_mutation("changeReceiver", "_id", ["input: ReceiverInput" => [
		"external_id" => $data["peer_id"],
		"external_type" => $data["peer_id"] == $data["from_id"] ? "personal_chat" : "group_chat",
		"external_system" => "vk",
		"title" => get_chat($data["peer_id"])["chat_settings"]["title"],
	]]);
	
	answer("Этот чат стал приемником");
}
elseif (preg_match("#^\/activate_source (.+)$#s", $data["text"], $matches))
{
	protopia_mutation("activateSourceReceiver", "_id", [
		"source_id: String" => getIdByLinkOrTitle($matches[1], "source"),
		"id: ID" => getReceiverByExternal(),
	]);
	
	answer("Вы подписались на источник" . "\n\n" . getSubscriptions());
}
elseif (preg_match("#^\/activate_theme (.+)$#s", $data["text"], $matches))
{
	protopia_mutation("changeTheme", "_id", [
		"_id: ID" => getIdByLinkOrTitle($matches[1], "theme"),
		"input: ThemeInput" => [
			"activate" => true,
		]
	]);

	answer("Вы подписались на тему" . "\n\n" . getSubscriptions());
}
elseif (preg_match("#^\/deactivate_source (.+)$#s", $data["text"], $matches))
{
	protopia_mutation("deactivateSourceReceiver", "_id", [
		"source_id: String" => getIdByLinkOrTitle($matches[1], "source"),
		"id: ID" => getReceiverByExternal(),
	]);

	answer("Вы отписались от источника" . "\n\n" . getSubscriptions());
}
elseif (preg_match("#^\/deactivate_theme (.+)$#s", $data["text"], $matches))
{
	protopia_mutation("changeTheme", "_id", [
		"_id: ID" => getIdByLinkOrTitle($matches[1], "theme"),
		"input: ThemeInput" => [
			"activate" => false,
		]
	]);

	answer("Вы отписались от темы" . "\n\n" . getSubscriptions());
}
elseif (preg_match("#^\/receiver_info$#s", $data["text"], $matches))
{
	$text = getSubscriptions();
	answer($text);
}
elseif (preg_match("#^\/share_receiver$#s", $data["text"], $matches))
{
	$receiver_id = getReceiverByExternal();
	$link = createLinkById($receiver_id);
	answer("#link{$link}");
}
elseif (preg_match("#^\/share_to (.+)$#s", $data["text"], $matches))
{
	protopia_mutation("shareSource", "_id", [
		"source_id: ID" => getSourceByExternal(),
		"receiver_id: ID" => getIdByLinkOrTitle($matches[1], "receiver"),
	]);
	
	answer("Вы поделились источником.");
}
elseif (preg_match("#^\/unshare_to (.+)$#s", $data["text"], $matches))
{
	protopia_mutation("unshareSource", "_id", [
		"source_id: ID" => getSourceByExternal(),
		"receiver_id: ID" => getIdByLinkOrTitle($matches[1], "receiver"),
	]);
	
	answer("Вы отозвали доступ к источнику.");
}
elseif (preg_match("#^\/share (.+)$#s", $data["text"], $matches))
{
	protopia_mutation("shareSource", "_id", [
		"source_id: ID" => getIdByLinkOrTitle($matches[1], "source"),
		"receiver_id: ID" => getReceiverByExternal(),
	]);
	
	answer("Вы поделились источником.");
}
elseif (preg_match("#^\/unshare (.+)$#s", $data["text"], $matches))
{
	protopia_mutation("unshareSource", "_id", [
		"source_id: ID" => getIdByLinkOrTitle($matches[1], "source"),
		"receiver_id: ID" => getReceiverByExternal(),
	]);
	
	answer("Вы отозвали доступ к источнику.");
}
elseif (preg_match("#^\/share (.+)$#s", $data["text"], $matches) && isset($data["message"]["reply_to_message"]))
{
	protopia_mutation("share", "_id", [
		"source_id: ID" => getIdByLinkOrTitle($matches[1], "source"),
		"receiver_id: ID" => getReceiverByExternal($data["message"]["reply_to_message"]["from"]),
	]);
	
	answer("Вы поделились источником.");
}
elseif (preg_match("#^\/unshare (.+)$#s", $data["text"], $matches) && isset($data["message"]["reply_to_message"]))
{
	protopia_mutation("unshare", "_id", [
		"source_id: ID" => getIdByLinkOrTitle($matches[1], "source"),
		"receiver_id: ID" => getReceiverByExternal($data["message"]["reply_to_message"]["from"]),
	]);
	
	answer("Вы отозвали доступ к источнику.");
}
elseif ($data["text"])
{
	$ecosystem_client_auth = true;
	protopia_mutation("addPost", "... on VkPost {_id}", [ "input: PostInput" => [
		"external_system" => "telegram",
		"post_text" => $data["text"],
		"post_id" => $data["conversation_message_id"],
		"post_time" => date('Y-m-d\TH:i:s.uP', $data["date"]),
		"external_user_id" => $data["from_id"],
		"user_first_name" => get_user($data["peer_id"], $data["from_id"])["profile"]["first_name"],
		"user_second_name" => get_user($data["peer_id"], $data["from_id"])["profile"]["last_name"],
		"user_display_name" => get_user($data["peer_id"], $data["from_id"])["profile"]["screen_name"],
		"external_id" => $data["peer_id"],
		"external_type" => $data["from_id"] == $data["peed_id"] ? "personal_chat" : "group_chat",
		"external_system" => "vk",
		"chat_title" => get_chat($data["peer_id"])["chat_settings"]["title"],
		"chat_display_name" => "",
		"external_post_link" => "",
	]]);
	$ecosystem_client_auth = false;
}

function getThemeIdByTitle($title)
{
	$themes = protopia_query("getThemes", "_id title");
	
	foreach ($themes as $theme)
	{
		if (mb_strtolower($theme["title"]) == mb_strtolower($title))
		{
			return $theme["_id"];
		}
	}
	
	return null;
}

function getSourceIdByTitle($title)
{
	$sources = protopia_query("getSources", "_id title");
	
	foreach ($sources as $source)
	{
		if (mb_strtolower($source["title"]) == mb_strtolower($title))
		{
			return $source["_id"];
		}
	}
	
	return null;
}

function getReceiverIdByTitle($title)
{
	$receivers = protopia_query("getReceivers", "_id title");
	
	foreach ($receivers as $receiver)
	{
		if (mb_strtolower($receiver["title"]) == mb_strtolower($title))
		{
			return $receiver["_id"];
		}
	}
	
	return null;
}

function getGroupIdByTitle($title)
{
	$sources = protopia_query("getGroups", "_id title");
	
	foreach ($sources as $source)
	{
		if (mb_strtolower($source["title"]) == mb_strtolower($title))
		{
			return $source["_id"];
		}
	}
	
	return null;
}

function getUserIdByVkId($user)
{
	global $ecosystem_client_auth;
	$ecosystem_client_auth = true;
	$result = protopia_query("getUserByExternalId", "_id", [
		"external_id: String" => $user,
		"external_system: String" => "vk",
	]);
	$ecosystem_client_auth = false;
	return $result["_id"];
}

function getReceiverByExternal()
{
	global $ecosystem_client_auth, $data;
	$ecosystem_client_auth = true;
	$result = protopia_query("getReceiverByExternal", "_id", [
		"external_id: String" => $data["peer_id"],
		"external_type: String" => $data["peer_id"] == $data["from_id"] ? "personal_chat" : "group_chat",
		"external_system: String" => "vk",
	]);
	
	$ecosystem_client_auth = false;
	return $result["_id"];
}
function getSourceByExternal()
{
	global $ecosystem_client_auth, $data;
	$ecosystem_client_auth = true;
	$result = protopia_query("getSourceByExternal", "_id", [
		"external_id: String" => $data["peer_id"],
		"external_type: String" => $data["peer_id"] == $data["from_id"] ? "personal_chat" : "group_chat",
		"external_system: String" => "vk",
	]);
	$ecosystem_client_auth = false;
	return $result["_id"];
}

function createLinkById($id)
{
	global $ecosystem_client_auth, $data;
	$ecosystem_client_auth = true;
	$result = protopia_mutation("createLinkById", "", [
		"id: ID!" => $id,
	]);
	$ecosystem_client_auth = false;
	return $result;
}

function getIdByLink($link)
{
	global $ecosystem_client_auth, $data;
	$ecosystem_client_auth = true;
	$result = protopia_query("getIdByLink", "", [
		"link: String!" => $link,
	]);
	answer($result);
	$ecosystem_client_auth = false;
	return $result;
}

function getIdByLinkOrTitle($string, $type)
{
	preg_match("/^#link(.+)$/", $string, $matches);
	if ($matches[1])
	{
		return getIdByLink($matches[1]);
	}
	if ($type == "source")
	{
		return getSourceIdByTitle($string);
	}
	if ($type == "theme")
	{
		return getThemeIdByTitle($string);
	}
	if ($type == "receiver")
	{
		return getReceiverIdByTitle($string);
	}
}

function getSubscriptions()
{
	global $data;
	
	$text = "";
	
	$receiver = protopia_query("getReceiver", "_id activate_sources {title} themes {title keywords activate}", ["id: ID!" => getReceiverByExternal()]);
	
	if ($receiver)
	{
		$text = "";
		if ($receiver["themes"])
		{
			$text .= "Темы:\n";
			foreach ($receiver["themes"] as $theme)
			{
				$status = $theme["activate"] ? "активна" : "неактивна";
				$text .= "----{$theme["title"]} (" . implode(", ", $theme["keywords"]) . ") [" . $status . "]\n";
			}
		}
		if ($receiver["activate_sources"])
		{
			$text .= "Активированные источники:\n";
			foreach ($receiver["activate_sources"] as $source)
			{
				$text .= "----{$source["title"]}\n";
			}
		}
	}
	if (!count($receiver["themes"]) && !count($receiver["activate_sources"]))
	{
		$text = "Подписки отсутствуют";
	}
	
	return $text;
}

session_write_close();