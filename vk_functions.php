<?php
define('VK_API_VERSION', '5.80'); //������������ ������ API 
define('VK_API_ENDPOINT', "https://api.vk.com/method/"); 
define('VK_API_ACCESS_TOKEN', $bot_token); 

//������� ��� ������ ������������� ������ API 
function _vkApi_call($method, $params = array()) { 
  $params['access_token'] = VK_API_ACCESS_TOKEN; 
  $params['v'] = VK_API_VERSION; 
  $url = VK_API_ENDPOINT.$method; 
  $curl = curl_init(); 
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
  curl_setopt($curl, CURLOPT_POST, 1);
  curl_setopt($curl, CURLOPT_POSTFIELDS,
				http_build_query($params)
			);
  $json = curl_exec($curl); 
  curl_close($curl); 
  $response = json_decode($json, true); 
  return $response['response']; 
}

function str_split_unicode($str, $l = 0) {
    if ($l > 0) {
        $ret = array();
        $len = mb_strlen($str, "UTF-8");
        for ($i = 0; $i < $len; $i += $l) {
            $ret[] = mb_substr($str, $i, $l, "UTF-8");
        }
        return $ret;
    }
    return preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
}

function answer($text, $buttons = null, $chat_id = null)
{
	if (false && mb_strlen($text) > 3000)
	{
		$texts = str_split_unicode($text, 3000);
		foreach ($texts as $text_part)
		{
			answer_one($text_part, $buttons, $chat_id, $parse_mode);
		}
	}
	else
	{
		return answer_one($text, $buttons, $chat_id, $parse_mode);
	}
}

function get_chat($chat_id)
{
	$filename = __DIR__ . "/chat_info/{$chat_id}.json";
	if (!file_exists($filename) || (time() - filemtime($filename) > 60 * 60))
	{
		$data = _vkApi_call('messages.getConversationsById', array( 
			'peer_ids' => $chat_id, 
			"extended" => 1,
			"fields" => "first_name, screen_name"
		));
		$data = $data["items"][0];
		$data["members"] = _vkApi_call('messages.getConversationMembers', array( 
			'peer_id' => $chat_id, 
			"fields" => "first_name, screen_name"
		));
		file_put_contents($filename, json_encode($data));
	}
	return json_decode(file_get_contents($filename), true);
}

function get_user($chat_id, $user_id)
{
	$chat = get_chat($chat_id);
	$result = [];
	foreach($chat["members"]["items"] as $item)
	{
		if ($item["member_id"] == $user_id)
		{
			$result["member"] = $item;
			break;
		}
	}
	foreach($chat["members"]["profiles"] as $profile)
	{
		if ($profile["id"] == $user_id)
		{
			$result["profile"] = $profile;
			break;
		}
	}
	return $result;
}

function answer_one($text, $buttons = null, $chat_id = null)
{
	global $data;
	if (!$chat_id)
	{
		$chat_id = $data["peer_id"];
	}
        $messages = array( 
		'peer_id' => $chat_id, 
		'message' => $text, 
	);
        if ($buttons) {
        $messages["keyboard"] = $buttons;    
        }
        if ($parse_mode) {
            
        }
	return _vkApi_call('messages.send', $messages); 
}

function tg_buttons_to_vk ($tg_keyboard) { //НЕ ДОПИСАНО, НЕ РАБОТАЕТ
    $tg_keyboard = array("inline_keyboard"=>array(
        array($is_public),
        array(array("text"=>"\xe2\x9e\x95Добавить новый приёмник", "callback_data"=>"6:")),
        array(array("text"=>"\xf0\x9f\x9a\xabРазорвать связь с приёмником", "callback_data"=>"8:")),
        array(array("text"=>"\xe2\x9d\x8cДеактивировать источник", "callback_data"=>"5:")),
        generate_back_buttons($id)
        ));
    
    foreach ($tg_keyboard["inline_keyboard"] as $key => $buttons_row) {
        foreach ($buttons_row as $button) {
            $buttons[$key];
        }
    }
    
    $vk_keyboard = array (
        "keyboard" => array (
            "onetime" => false,
            "buttons" => $buttons,
            "inline" => true,
        )
    );
            
    return json_encode($vk_buttons);
}