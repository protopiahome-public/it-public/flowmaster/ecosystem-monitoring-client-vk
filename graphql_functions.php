<?php

require_once __DIR__ . '/vendor/autoload.php';

//use \Firebase\JWT\JWT;
use Jose\Object\JWK;
use Jose\Factory\JWSFactory;
$ecosystem_token = "";

function protopia_query($method, $results, $params_variables = null)
{
	$variables = [];
	$results = $results ? "{" . $results . "}" : $results;
	if ($params_variables)
	{
		foreach ($params_variables as $key => $value)
		{
			$key = trim($key);
			$key = explode(":", $key);
			$key[0] = trim($key[0]);
			$key[1] = trim($key[1]);
			$variables[$key[0]] = $value;
			$params1[] = "$" . $key[0] . ":" . $key[1];
			$params2[] = $key[0] . ": $" . $key[0];
		}
		$params1 = implode(", ", $params1);
		$params2 = implode(", ", $params2);
	
		$query = <<<EOF
query ({$params1}) {
	{$method} ({$params2}) 
		{$results}
}
EOF;
	}
	else
	{
		$query = <<<EOF
query {
	{$method} 
		{$results}
}
EOF;
	}
	//echo $query;
	return protopia_graphql($query, $variables);
}

function protopia_mutation($method, $results, $params_variables = null)
{
	$variables = [];
	foreach ($params_variables as $key => $value)
	{
		$key = trim($key);
		$key = explode(":", $key);
		$key[0] = trim($key[0]);
		$key[1] = trim($key[1]);
		$variables[$key[0]] = $value;
		$params1[] = "$" . $key[0] . ":" . $key[1];
		$params2[] = $key[0] . ": $" . $key[0];
	}
	$params1 = implode(", ", $params1);
	$params2 = implode(", ", $params2);
	
	$results = $results ? "{" . $results . "}" : $results;
	
	$query = <<<EOF
mutation ({$params1}) {
	{$method} ({$params2}) 
		{$results}
}
EOF;
	return protopia_graphql($query, $variables);
}

function protopia_auth()
{
	global $ecosystem_token, $ecosystem_client_id, $ecosystem_client_secret, $ecosystem_user_token, $data, $jwt_key, $assertion_jwt;
	
	$assertion_token = array(
		"sub" => $ecosystem_client_id,
		"aud" => ["http://monitoring-vk.kb.protopia-home.ru", "http://monitoring-vk.kb.protopia-home.ru"],
		"iss" => "http://monitoring-vk.kb.protopia-home.ru",
		"iat" => time(),
		"exp" => time() + 3600,
	);
	//$assertion_jwt = JWT::encode($assertion_token, $ecosystem_client_secret);
	//$jws = new \Gamegos\JWS\JWS();
	//$assertion_jwt = $jws->encode(['alg' => 'HS256', "kid" => "22EEDJpPhWWTrcwvTy-tt5N-Wa7rA2EOrUa3sUO8wHI"], $assertion_token, $ecosystem_client_secret);
	$key = new JWK([
		'kty' => 'oct',
		'k'   => $ecosystem_client_secret,
	]);
	$key2 = new JWK([
		'kty' => 'oct',
		'k'   => $ecosystem_client_secret,
	]);
	$assertion_jwt = JWSFactory::createJWSToCompactJSON(
		$assertion_token,                      // The payload or claims to sign
		$key,                         // The key used to sign
		['alg' => 'HS256', 
		"kid" => $ecosystem_client_id
		]
	);
	//answer(print_r(['alg' => 'HS256', "kid" => "22EEDJpPhWWTrcwvTy-tt5N-Wa7rA2EOrUa3sUO8wHI"], true));
	//answer(print_r($assertion_token, true));
//answer($ecosystem_client_secret);

	$id_token = array(
		"sub" => $data["from_id"],
		"aud" => ["http://monitoring-vk.kb.protopia-home.ru", "http://monitoring-vk.kb.protopia-home.ru"],
		"iss" => "http://monitoring-vk.kb.protopia-home.ru",
		"iat" => time(),
		"exp" => time() + 3600,
		"acr" => "vk",
	);
	//$id_jwt = JWT::encode($id_token, $ecosystem_client_secret);
	
	
	$id_jwt = JWSFactory::createJWSToCompactJSON(
		$id_token,                      // The payload or claims to sign
		$key2,                         // The key used to sign
		['alg' => 'HS256', "kid" => $ecosystem_client_id]
	);
	
	$auth_result = protopia_mutation("authorize", "auth_req_id", ["input: AuthorizeInput!" => 
		array(
			"scope" => ["user"],
			"id_token_hint" => $id_jwt,
			"assertion" => $assertion_jwt,
		)
	]);
	
	$token_user_result = protopia_mutation("token", "access_token", ["input: TokenInput!" => 
		array(
			"grant_type" => "ciba",
			"auth_req_id" => $auth_result["auth_req_id"],
			"assertion" => $assertion_jwt,
		)
	]);
	
	
	$token_result = protopia_mutation("token", "access_token", ["input: TokenInput!" => 
		array(
			"grant_type" => "jwt-bearer",
			"assertion" => $assertion_jwt,
		)
	]);
	
	$ecosystem_user_token = $token_user_result["access_token"];
	$ecosystem_token = $token_result["access_token"];
	//answer(print_r($token_result, true));
	//answer(print_r($token_user_result, true));
}

function protopia_graphql($query, $variables = [])
{
	global $ecosystem_client_auth, $ecosystem_token, $ecosystem_addr, $ecosystem_user_token;
	
	return graphql_query($ecosystem_addr, $query, $variables, $ecosystem_client_auth || !$ecosystem_user_token ? $ecosystem_token : $ecosystem_user_token);
}

function graphql_query($endpoint, $query, $variables = [], $token = null)
{
	global $ecosystem_client_secret, $jwt_key, $data, $ecosystem_client_auth;
	
    $headers = ['Content-Type: application/json', 'User-Agent: Dunglas\'s minimal GraphQL client'];
    if ($token) {
        $headers[] = "Authorization: Bearer $token";
    }
	else {
		//$headers[] = "Authorization: Basic {$ecosystem_client_secret}";
	}
	//answer(print_r($headers, true));
	//answer(print_r(['query' => $query, 'variables' => $variables], true));
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$endpoint);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['query' => $query, 'variables' => $variables]));
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$graphql_data = curl_exec($ch);
	curl_close ($ch);
    $result = json_decode($graphql_data, true);
	
    if (isset($result["errors"]) || !isset($result["data"])) {
		return;
		if ($result["errors"][0]["extensions"]["code"] != "INTERNAL_SERVER_ERROR")
		{
			//throw new Exception($result);
		}
		echo "<pre>";
		$error_text = "";
		foreach ($result["errors"] as $error)
		{
			$error_text .= $error["message"] . "\n";
		}
		$error_text .= print_r($result, true);
		echo $error_text;
		echo "</pre>";
		answer(json_encode(['query' => $query, 'variables' => $variables]));
		answer($error_text);
    }
	$result = $result["data"];
	return reset($result);
}